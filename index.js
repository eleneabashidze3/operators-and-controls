module.exports.arrayDiff = function arrayDiff(array1, array2) {
    const diff = [];
    const longArray = array1.length >= array2.length ? array1 : array2;
    const shortArray = array1.length >= array2.length ? array2 : array1;

    for (const el of longArray) {
        if (!shortArray.includes(el)) {
            diff.push(el);
        }
    }
    for (const b of shortArray) {
        if (!longArray.includes(b)) {
            diff.push(b);
        }
    }
    return diff;

}



module.exports.evenOrOdd = function evenOrOdd(x, y) {
    let answerArr = [];
    for (let i = x; i <= y; i++) {
        if (i % 2 == 0) {
            answerArr.push('${i} is even');
        } else {
            answerArr.push('${i} is odd');
        }
    }
    return answerArr;
}





module.exports.rangeSum = function rangeSum(x, y) {
    let summed = 0;
    for (let i = x; i <= y; i++) {
        summed += i;
    }
    return summed;
}
